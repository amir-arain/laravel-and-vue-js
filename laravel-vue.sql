-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 11, 2019 at 07:59 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-vue`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `client_status_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clients_deleted_at_index` (`deleted_at`),
  KEY `5344_5b20f635248a7` (`client_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `first_name`, `last_name`, `company_name`, `email`, `phone`, `website`, `skype`, `country`, `created_at`, `updated_at`, `deleted_at`, `client_status_id`) VALUES
(1, 'Stephania Moore', 'Vickie Farrell', 'Dr. Kenneth Hartmann V', 'wjast@example.com', 'Jeffery Keeling', 'Mrs. Cathy Rippin Sr.', 'Mr. Alexis Cassin', 'Prof. Edison Gulgowski I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 1),
(2, 'Dr. Antoinette Rippin Jr.', 'Miss Virgie Bayer', 'Arjun Langosh', 'joseph.mckenzie@example.net', 'Mrs. Palma Volkman III', 'Mr. Dylan Bechtelar', 'Mrs. Macie Keebler', 'Keshaun Cummerata', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 2),
(3, 'Leta Toy', 'Prof. Colt Koelpin Jr.', 'Edmund Hyatt', 'nwiza@example.net', 'Evert O\'Kon', 'Justen Koch DDS', 'Mr. Thurman Purdy II', 'Anita Wisozk', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 3),
(4, 'Miss Rosalia Bartell', 'Thelma Rodriguez', 'Mrs. Kailey Stehr', 'fbins@example.net', 'Dr. Leopold Cremin I', 'Prof. Alexanne Pouros IV', 'Prof. Alford Kuvalis', 'Mrs. Constance Mitchell', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 4),
(5, 'Miss Vella Balistreri Jr.', 'Verner Casper', 'Ernesto Casper Jr.', 'douglas.darron@example.org', 'Dr. Celine Murazik', 'Giovanni O\'Hara', 'Clara Hills', 'Miss Libby Brown DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 5),
(6, 'Prof. Freeman Predovic MD', 'Theodore Schulist', 'Jayden Ledner', 'king.isabelle@example.com', 'George Wisozk III', 'Madisen Hane', 'Bertrand Gusikowski', 'Brionna Walker MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 6),
(7, 'Prof. Neoma Casper', 'Rowan Tromp', 'Pierre Feest', 'elna36@example.org', 'Prof. Stacy Gorczany I', 'Theodora Walker III', 'Dr. Darrin Schamberger', 'Carson Carter', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 7),
(8, 'Dr. Sonia Veum', 'Prof. Thad Harber PhD', 'Prof. Donavon Barton', 'stuart27@example.net', 'Orion Beier PhD', 'Keyshawn Mann', 'Mr. Ransom Considine MD', 'Otis Boyle', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 8),
(9, 'Dr. Thaddeus Conn DVM', 'Laurianne Rosenbaum', 'Yadira Schinner', 'mfriesen@example.org', 'Prof. Boris Heaney', 'Miss Leatha Cormier III', 'Dr. Bill Stiedemann Jr.', 'Khalid Barton', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 9),
(10, 'Porter Weimann', 'Tyrell O\'Keefe', 'Mr. Louvenia Smitham', 'borer.rosalia@example.com', 'Weston Kshlerin', 'Genevieve Nitzsche', 'Ryder Reinger', 'Dorothy Herzog', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 10),
(11, 'Lauryn Zboncak Sr.', 'Ida Crooks', 'Dr. Cayla Pouros MD', 'zzieme@example.com', 'Anabelle Gutmann', 'Ms. Rosalia Luettgen', 'Kathryn Jast', 'Dr. Daniela Trantow', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 11),
(12, 'Maurice Legros', 'Josianne Mueller', 'Ebony Schinner', 'mayer.rae@example.com', 'Nakia Schaefer', 'Irma Koepp', 'Delaney Wehner IV', 'Mr. Lance Zboncak', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 12),
(13, 'Ms. Lilly Hansen IV', 'Max Grimes', 'Dawson Sanford Sr.', 'joany.nitzsche@example.org', 'Ethelyn Gerhold', 'Mr. Dejuan Waelchi', 'Emilie Ritchie', 'Enoch Brekke Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 13),
(14, 'Dr. Albert O\'Reilly III', 'Prof. Willy Leuschke', 'Vance Hagenes', 'qstroman@example.net', 'Miss Marilie Rohan', 'Wilmer Moore', 'Mrs. Carissa Sporer', 'Agustina Graham', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 14),
(15, 'Xavier Hettinger DDS', 'Sedrick Homenick', 'Miss Kavon Balistreri I', 'lazaro.towne@example.net', 'Major O\'Connell', 'Annabell Murazik DVM', 'Ms. Fae Runte DVM', 'Dr. Maverick Predovic', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 15),
(16, 'Paris Emard', 'Fletcher Gorczany', 'Tyrel Dickinson', 'hkihn@example.org', 'Prof. Leonie Roob Jr.', 'Estevan Littel', 'Miss Elisa Goyette', 'Pete Dickens', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 16),
(17, 'Hailee Towne', 'Eldred Olson', 'Geovanny Lynch', 'sbuckridge@example.org', 'Mrs. Jailyn O\'Keefe IV', 'Mr. Savion Crooks IV', 'Miss Mertie Zulauf III', 'Marge Howell PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 17),
(18, 'Prof. Noah Bode', 'Gianni Ullrich', 'Van Zieme', 'ahmad.purdy@example.net', 'Aryanna Block III', 'Pedro Pollich', 'Esperanza Renner', 'Prof. Dee Collins', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 18),
(19, 'Desiree Heaney', 'Lucienne Veum', 'Delia Balistreri', 'dulce91@example.net', 'Michelle Kozey', 'Dr. Ollie Kerluke', 'Ms. Bailee Collins PhD', 'Arely Steuber IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 19),
(20, 'Karl Hagenes V', 'Prof. Demarco Waelchi', 'Lavonne Gottlieb MD', 'audra.feest@example.net', 'Ms. Shany Beatty V', 'Cathryn Satterfield', 'Elmore Koch', 'Emelie Kessler', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 20),
(21, 'Vicenta King', 'Miss Maud Lebsack Jr.', 'Dr. Jordy Osinski', 'fay.neal@example.com', 'Jason Tillman', 'Prof. Vernie Ondricka', 'Carole Runolfsson DDS', 'Cristian Ritchie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 21),
(22, 'Hanna Wintheiser I', 'Green Kuvalis', 'Mr. Muhammad Doyle MD', 'lisette32@example.com', 'Timmothy Adams', 'Lola Bergnaum', 'Dr. Tyrell Murazik', 'Amir Watsica PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 22),
(23, 'Mr. Anderson Ebert', 'Dandre Howell', 'Prof. Jake Powlowski IV', 'isaac68@example.com', 'Joyce Runolfsson', 'Dr. Salvador Brekke', 'Loren Nitzsche', 'Fred Nicolas', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 23),
(24, 'Prof. Jeramy Ernser', 'Dr. Callie Mayert PhD', 'Prof. Mertie DuBuque I', 'ned15@example.net', 'George Bahringer II', 'Ms. Zena Thiel PhD', 'Miss Mallie Ziemann', 'Rene Murray', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 24),
(25, 'Wilford Reilly', 'Robbie Klein', 'Dr. Abel Crona', 'kilback.patrick@example.net', 'Prof. Jazlyn Wisoky IV', 'Marianne Champlin Jr.', 'Lavina Hartmann', 'Duncan Huel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 25),
(26, 'Edythe Kreiger', 'Roscoe Crist', 'Freddie Feil', 'carley.champlin@example.org', 'Eldon Green', 'Dr. Kaley Gleichner Jr.', 'Dr. Brain Johnson', 'Dr. Adrienne Schamberger Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 26),
(27, 'Reymundo Bernhard MD', 'Mr. Loyal Balistreri IV', 'Dr. Kieran Rice', 'kris95@example.com', 'Joaquin Schuppe', 'Cora Hoppe Sr.', 'Ms. Bonita Connelly PhD', 'Mr. Rickey Larson V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 27),
(28, 'Mrs. Amber Erdman DVM', 'Aaliyah Goodwin', 'Bartholome Rau III', 'aurore.spencer@example.net', 'Mafalda Mertz', 'Garrick Waters', 'Prof. Izabella Boyle III', 'Selina Halvorson', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 28),
(29, 'Reynold Wuckert PhD', 'Angeline Volkman', 'Vallie Bosco PhD', 'mia69@example.com', 'Aubree Bergstrom', 'Mariam Daugherty', 'Nikko Gutkowski', 'Miss Savanah Baumbach', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 29),
(30, 'Sedrick Upton', 'Lera Homenick', 'Miss Haven Block', 'muriel.padberg@example.com', 'Prof. Petra Smith III', 'Enos Schaden', 'Franco Walter', 'Brionna Hegmann MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 30),
(31, 'Lisa Lind', 'Dr. Craig Lemke', 'Prof. Jettie Runte III', 'ferry.lauryn@example.org', 'Dr. Elvie Langosh', 'Caleigh Osinski', 'Ms. Pink Wisoky', 'Dr. Nickolas Murray', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 31),
(32, 'Prof. Bryon Crooks MD', 'Clay Fahey IV', 'Prof. Anastasia Lubowitz Sr.', 'myrtis30@example.org', 'Miss Shyanne Reinger', 'Maudie Bosco', 'Lisandro Rath', 'Vilma Mitchell DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 32),
(33, 'Kris Koelpin', 'Prof. Jolie Turner PhD', 'D\'angelo Thompson', 'kelli01@example.org', 'Jaden Kuhic', 'Xavier Daniel', 'Jaycee Farrell III', 'Dr. Freeman Flatley III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 33),
(34, 'Kirsten Herman', 'Vicente Dickens MD', 'Lizeth Christiansen DVM', 'domenica04@example.org', 'Sunny Beatty', 'Daphnee Macejkovic', 'Prof. Maegan Kessler', 'Bernie Pagac', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 34),
(35, 'Mrs. Jacquelyn Bechtelar', 'Ted Jakubowski', 'Tomas Dibbert', 'alabadie@example.net', 'Giuseppe Becker', 'Alexane Gerlach', 'Helen Nader', 'Mrs. Alexanne Lindgren IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 35),
(36, 'Clarabelle Goodwin', 'Nadia Heaney DVM', 'Arturo Weber Sr.', 'frice@example.net', 'Dr. Willis Hagenes', 'Prof. Adolfo Purdy', 'Aurelie Cartwright', 'Prof. Cali Feest', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 36),
(37, 'Miss Penelope Price Jr.', 'Meagan Haag III', 'Braulio Hayes', 'lehner.lamar@example.org', 'Louie Bednar', 'Kasandra Kuhic', 'Mr. Liam Rodriguez', 'Prof. Niko Lesch I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 37),
(38, 'Renee DuBuque', 'Hayley Williamson III', 'Lynn Bogan', 'lbreitenberg@example.com', 'Ida Rodriguez', 'Lazaro Stokes', 'Tia Bergstrom', 'Mrs. Briana Zieme III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 38),
(39, 'Prof. Felicita Osinski MD', 'Dr. Theron Cormier PhD', 'Arturo Kirlin', 'guillermo.botsford@example.org', 'Bernice Roob', 'Clotilde Cremin', 'Prof. Margarette Glover I', 'Jody Hessel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 39),
(40, 'Bennie Sauer', 'Prof. Rosamond Lebsack', 'Dr. Isac Welch DVM', 'hettinger.timothy@example.org', 'Antwon Blick', 'Jovan Klein', 'Norris Altenwerth IV', 'Linnie Purdy', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 40),
(41, 'Toney Mueller', 'Eliseo Bartoletti', 'Celestino Rath Jr.', 'kemmer.thad@example.com', 'Miss Lorna Kiehn PhD', 'Mylene Grady', 'Dr. Camden Robel Sr.', 'Quincy Cassin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 41),
(42, 'Dr. Dixie Eichmann', 'Alexander Hills', 'Connor Goldner', 'haley.maybell@example.com', 'Eleanora Yundt', 'Mr. Abdullah Wiza V', 'Mrs. Lelah Jast', 'Dr. Reta Gerlach', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 42),
(43, 'Rachael Buckridge', 'Shane Kuphal', 'Dr. Ignatius Beahan', 'madalyn51@example.net', 'Mrs. Oma Powlowski PhD', 'Lea Jaskolski', 'Monroe Miller DVM', 'Aaron D\'Amore', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 43),
(44, 'Susan O\'Reilly', 'Lester Pouros', 'Ashlee Veum', 'everardo09@example.org', 'Dr. Blanche Kuphal', 'Prof. Lexus Hoppe PhD', 'Shanna Wuckert', 'Mr. Reilly Wunsch III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 44),
(45, 'Mrs. Pasquale Schamberger V', 'Delpha Ferry Sr.', 'Mr. Jayde Ruecker DVM', 'mattie.turcotte@example.com', 'Mr. Darrion Kshlerin', 'Dr. Karson Wuckert', 'Jaeden Wilderman', 'Billy Volkman II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 45),
(46, 'Niko Reilly MD', 'Kacey Schowalter', 'Theodore Harris', 'braxton90@example.org', 'Prof. Tamara Grady', 'Concepcion Bins', 'Walton Turcotte', 'Dr. Toy Hodkiewicz', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 46),
(47, 'Bernita Hyatt III', 'Miss Summer Tillman IV', 'Amara Erdman', 'shanahan.junior@example.net', 'Raina Nader', 'Johnnie Farrell Sr.', 'Iva Wolf MD', 'Prof. Juanita Veum', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 47),
(48, 'Colten Marquardt III', 'Hettie Leffler', 'Aaliyah Cassin', 'jhuels@example.net', 'Pasquale Goodwin', 'Delilah Heidenreich', 'Prof. Ida Volkman', 'Cleve Kilback', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 48),
(49, 'Cortez Jast DDS', 'Pietro Macejkovic', 'Bernadette Tromp', 'coby67@example.net', 'Maureen Blanda', 'Stephanie Morar', 'Minerva Ferry', 'Seamus Wehner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 49),
(50, 'Kayden Zulauf', 'Miss Lavada Konopelski', 'Mr. Jayme Marks IV', 'kennedi.mills@example.org', 'Lula Krajcik', 'Berneice McLaughlin II', 'Jabari Dare', 'Dr. Tara Kulas', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 50),
(51, 'Taurean Windler', 'Rene Romaguera DVM', 'Dr. Manuela Schroeder', 'senger.roberta@example.org', 'Ted McKenzie I', 'Eda Wiza', 'Alanis Senger', 'Prof. Garrett Turcotte', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 51),
(52, 'Dr. Kurt Dibbert', 'Viola Denesik I', 'Juston Pollich', 'cbergnaum@example.com', 'Waldo Hodkiewicz', 'Ramiro Borer', 'Neoma Hodkiewicz', 'Rozella Ruecker', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 52),
(53, 'Angelica Kuphal Sr.', 'Bernard Gutkowski', 'Lyric Oberbrunner', 'anika81@example.com', 'Prof. Chaim Abbott', 'Prof. Amiya Mayert', 'Alayna Marks', 'Crawford McClure', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 53),
(54, 'Issac Stanton', 'Tyreek King', 'Brook Feest', 'georgianna80@example.net', 'Eloy Gibson', 'Dr. Eveline Heaney DVM', 'Fay Maggio', 'Godfrey Rau', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 54),
(55, 'Prof. Dagmar Crona III', 'Erich Beahan', 'Brian Schuster', 'laila.parisian@example.org', 'Marcelo Gerhold I', 'Jaquelin Gottlieb', 'Prof. Neil Goyette', 'Brayan Herman', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 55),
(56, 'Berenice Strosin', 'Miss Aisha Herman III', 'Susana Jacobs', 'rjones@example.org', 'Maegan Morar', 'Mr. Ellis Blanda', 'Dr. Estrella Jacobs', 'Prof. Henry Gulgowski', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 56),
(57, 'Miss Myrtie McGlynn', 'Elwin Gibson', 'Rosalind Kerluke', 'dayton.kuhn@example.net', 'Miss Maci Torp', 'Selmer McCullough I', 'Dr. Garfield Schaefer', 'Macy Wiegand', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 57),
(58, 'Maxime Conroy V', 'Dr. Ocie Keebler', 'Darby Runolfsdottir', 'rylan.ortiz@example.net', 'Jane Padberg', 'Tyrel Schroeder', 'Wilton Barrows', 'Erick Dibbert', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 58),
(59, 'Dr. Kayli Farrell', 'Nick Feeney', 'Fidel Medhurst', 'jones.donny@example.org', 'Bonnie Roberts', 'Ms. Jackie Daugherty', 'Wilmer Schaefer I', 'Braden Smith II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 59),
(60, 'Dr. Floy Crist', 'Alyson Wilkinson', 'Riley Kuphal', 'dare.dario@example.com', 'Freeman Gusikowski', 'Neoma Hessel', 'Miss Sallie Kuhic II', 'Fletcher Welch', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 60),
(61, 'Zoila Dach', 'Ocie Cruickshank', 'Dr. Stephanie McClure', 'cdenesik@example.org', 'Milan Hilll', 'Maximo Hoeger', 'Gloria Gislason DDS', 'Richard Witting', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 61),
(62, 'Carissa Gleason DDS', 'Sebastian Will', 'Felix Larkin', 'tbartell@example.org', 'Mr. Hilario Hane DDS', 'Dr. Jonathon Cole IV', 'Prof. Albin Bednar', 'Laisha Medhurst', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 62),
(63, 'Shyann Osinski', 'Watson Larson', 'Prof. Dayton Reynolds MD', 'asia03@example.net', 'Adriana Ledner Sr.', 'Bernardo Lesch', 'Brice Abernathy', 'Prof. Dovie Hoppe II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 63),
(64, 'Niko Kuphal', 'Viviane Metz', 'Dr. Lavern Hudson', 'goyette.bertram@example.net', 'Norbert Ledner', 'Ms. Wanda Auer Sr.', 'Violette Legros', 'Anderson Schamberger', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 64),
(65, 'Owen Marks III', 'Prof. Danyka Gutmann DVM', 'Janice Simonis Sr.', 'dickens.elmore@example.net', 'Prof. Malinda Ruecker', 'Gregorio Marks', 'Aaron Mayert I', 'Prof. Claude Wisozk', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 65),
(66, 'Floyd Stokes', 'Hailey Miller', 'Eveline Jenkins', 'dgrady@example.org', 'Mr. Lucius Mraz', 'Prof. Cleo Bechtelar', 'Prof. Tiana Halvorson', 'Kyla Kuhlman IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 66),
(67, 'Prof. Selmer Carroll', 'Mr. Freeman Bartoletti', 'Prudence Schuppe PhD', 'florencio.king@example.com', 'Marques Dooley', 'Clair Wisoky', 'Margret Brekke', 'Ethyl Moen', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 67),
(68, 'Otha Lang IV', 'Tessie Blick', 'Miss Sienna Auer', 'barbara44@example.net', 'Mr. Lucas Marks I', 'Paris Kshlerin', 'Miss Leslie Mills I', 'Manley Quitzon MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 68),
(69, 'Blanche Homenick Jr.', 'Nya Friesen II', 'Nikki Schmitt', 'vada.lakin@example.com', 'Celestino Farrell Sr.', 'Meagan Cruickshank', 'Ms. Violette Balistreri', 'Charlotte Kihn', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 69),
(70, 'Mr. Marcelino McClure', 'Sean Wolf', 'Mr. Donnell Williamson IV', 'terrance71@example.org', 'Ashleigh Block', 'Leatha Armstrong', 'Miss Aurelie Haley', 'Arnoldo Romaguera III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 70),
(71, 'Dr. Bryana Gerhold', 'Arvel Lockman', 'Alisa Doyle', 'mlabadie@example.org', 'Ms. Shyanne DuBuque', 'Rebekah Buckridge', 'Grant Gutmann DVM', 'Kaylin Price', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 71),
(72, 'Cynthia Schmitt', 'Jaren Batz', 'Idell Turcotte III', 'samantha86@example.org', 'Dr. Trenton Kris', 'Mollie Stamm Jr.', 'Mr. Henry Schneider V', 'Zechariah Block PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 72),
(73, 'Mylene Marks', 'Viva Schumm', 'Rowena Skiles', 'wilson43@example.com', 'Selmer Hirthe', 'Cassie Collins Jr.', 'Ms. Kathlyn Rowe PhD', 'Sheila Jones MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 73),
(74, 'Emely West', 'Miracle Ortiz', 'Kelley Cremin', 'gusikowski.olin@example.net', 'Mabelle Nader MD', 'Miss Marianne Considine', 'Dr. Jaclyn Lindgren Sr.', 'Prof. Jana Bergnaum IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 74),
(75, 'Tara Kuhlman', 'Shemar Parker', 'Dr. Brooklyn Kirlin IV', 'general40@example.net', 'Corrine Jakubowski I', 'Miss Damaris Johnston', 'Miss Maye Zulauf', 'Florence Weissnat II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 75),
(76, 'Jeromy Kling', 'Marisol Schinner', 'Jody Schoen', 'eklein@example.org', 'Prof. Bradley Turcotte PhD', 'Marcelino Adams', 'Berenice Gibson', 'Blaise Kihn V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 76),
(77, 'Afton Shanahan III', 'Glenda Goldner', 'Michel Hirthe', 'ekassulke@example.com', 'Ms. Diana Thiel PhD', 'Mrs. Charlotte Satterfield DDS', 'Prof. Caesar Pouros', 'Prof. Adolph Bosco V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 77),
(78, 'Autumn Gleason', 'Prof. Doyle Runolfsson', 'Dr. Elena Carter', 'mstamm@example.com', 'Emmanuel Dickinson', 'Prof. Olga Beer', 'Precious Koch', 'Miss Daisha Leannon', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 78),
(79, 'Gustave Streich V', 'Ms. Lillie Zboncak', 'Salma Kunde', 'halvorson.marianne@example.com', 'Melvin Harris', 'Esmeralda Swaniawski MD', 'Jarvis Kilback PhD', 'Prof. Orrin Wiza', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 79),
(80, 'Markus Huel', 'Dr. Elvis Hermann DDS', 'Antonia Green', 'mario73@example.net', 'Archibald Bradtke', 'Nigel Fahey', 'Ms. Duane Schamberger', 'Abdul Gibson', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 80),
(81, 'Nicholaus Keeling', 'Laney Metz', 'Aubree McDermott DDS', 'alec35@example.org', 'Aileen Legros', 'Miss Francesca Jacobi', 'Luisa Schmeler', 'Ms. Yvonne Cummings MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 81),
(82, 'Garrison Hudson', 'Larry Wilderman', 'Amos Mante', 'travon49@example.net', 'Diana Ratke', 'Rosina Klein IV', 'Mrs. Deanna Macejkovic Jr.', 'Orland Stokes', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 82),
(83, 'Rosanna Gerlach', 'Khalil Pouros', 'Prof. Emanuel Rempel', 'della75@example.com', 'Jayde McKenzie MD', 'Mr. Kyle Hartmann Sr.', 'Eliseo Kris', 'Angeline Nader Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 83),
(84, 'Carmelo Howe', 'Jammie Ullrich', 'Demetris Bayer I', 'csteuber@example.net', 'Amari McGlynn', 'Aliyah Blick', 'Miss Darby Casper Jr.', 'Vicente Rosenbaum', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 84),
(85, 'Neha Mraz', 'Pearl Trantow', 'Orion Rutherford', 'satterfield.emiliano@example.org', 'Mackenzie Mohr', 'Margot Lebsack', 'Elva Lubowitz', 'Ettie Glover', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 85),
(86, 'Dannie Vandervort', 'Jerrold Sauer', 'Prof. Cristal Lemke', 'nikita.maggio@example.org', 'Miss Meagan Greenholt', 'Greg Schmidt', 'Vicente Schmidt', 'Bernard Runolfsson III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 86),
(87, 'Mrs. Missouri O\'Reilly', 'Garfield Kessler', 'Vladimir Mills', 'forrest.greenfelder@example.com', 'Prof. Felton Moen V', 'Oswald Lesch', 'Sven Morissette', 'Kurtis Hettinger DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 87),
(88, 'Kelsie King', 'Jovani Kris', 'Samanta Kunze', 'quitzon.sydnee@example.net', 'Toby Zboncak', 'Prof. Eldora Rice IV', 'Alize Rippin', 'Fatima Bahringer Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 88),
(89, 'Jamaal Hintz', 'Faye Romaguera', 'Ludwig Conn', 'newell03@example.net', 'Julie Daugherty', 'Ali Trantow', 'Bettye Bartell', 'Dr. Llewellyn Wolf I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 89),
(90, 'Prof. Gregorio Kemmer', 'Prof. Ronaldo Koch I', 'Yvette Bernhard', 'rempel.darrick@example.com', 'Vinnie Rohan Jr.', 'Corine Mertz', 'Maximus Metz', 'Miss Lulu Donnelly III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 90),
(91, 'Bridgette Nolan DDS', 'Germaine Feil', 'Leif Walter', 'carmel.braun@example.com', 'Quinton Hills I', 'Wiley Kuvalis', 'Colton Pagac DVM', 'Dr. Green Nikolaus DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 91),
(92, 'Shaylee Prosacco', 'Federico Leuschke', 'Miss Ona Rohan V', 'cole.madisen@example.org', 'Nayeli Jerde', 'Evalyn Ferry', 'Afton Kuphal', 'Priscilla Kunze', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 92),
(93, 'Deborah Kuhic', 'Russ Sanford', 'Devin Orn', 'raquel.trantow@example.net', 'Verona Hettinger', 'Ms. Arlie Welch DDS', 'Sadie Parker', 'Johathan Nicolas', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 93),
(94, 'Delphine Kling', 'Kendrick Hahn', 'Miss Laury Feeney', 'khintz@example.com', 'Berta Ziemann V', 'Chester Littel', 'Providenci Sauer Sr.', 'Shanie Goodwin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 94),
(95, 'Erika Goyette III', 'Ryan Davis II', 'Margret Fritsch', 'grayson34@example.net', 'Marques Nitzsche PhD', 'Julianne Deckow', 'Noah Reichert', 'Allene Sporer', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 95),
(96, 'Mr. Misael Spencer MD', 'Maxwell Balistreri DVM', 'Araceli Herzog', 'yasmeen.ondricka@example.com', 'Liam Breitenberg', 'Ellsworth Hane', 'Alyson Greenholt', 'Carole Wilkinson', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 96),
(97, 'Hunter Runolfsson PhD', 'Dr. Karson Roberts', 'Rafaela Rodriguez', 'cartwright.frances@example.org', 'Dr. Charles Bashirian DDS', 'D\'angelo Feil', 'Miss Danyka Gulgowski V', 'Hallie Fahey', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 97),
(98, 'Benton Schmidt', 'Adelbert Beier', 'Alexa Christiansen', 'glesch@example.com', 'Prof. Mariane Roberts MD', 'Elmer Von', 'Billie Grady', 'Alvera Schmitt', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 98),
(99, 'Tess Runolfsdottir', 'Orion Howe', 'Cleta Monahan', 'walsh.leslie@example.net', 'Cary Kihn', 'Mr. Marcos Yost V', 'Prof. Marcus Schmidt', 'Megane Hermann', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 99),
(100, 'Tara Deckow', 'Kasandra White', 'Lon Block', 'tatyana54@example.com', 'Euna Lowe', 'Franz Stracke', 'Maritza Hilpert', 'Dr. Bulah Hagenes', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 100),
(101, 'Mr. Watson Gislason', 'Emilio Brekke', 'Elijah Boehm', 'arno99@example.net', 'Ruben Bailey MD', 'Dr. Darrel Tillman', 'Ethel Pouros', 'Ms. Donna Zieme III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 101),
(102, 'Maryjane Rodriguez', 'Devonte Schiller', 'Bradley Balistreri', 'lavada.koelpin@example.com', 'Ms. Muriel Smith', 'Justina Cremin', 'Mrs. Eryn Kerluke DVM', 'Miguel Stoltenberg', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 102),
(103, 'Eladio Satterfield', 'Dr. Sidney Jacobs DDS', 'Mr. Mitchel Bartoletti V', 'wendell.blanda@example.com', 'Hellen Donnelly', 'Nikolas Wiegand', 'Jeff Pollich', 'Ahmed Bauch', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 103),
(104, 'Willy Klocko', 'Wiley Bashirian', 'Miss Delphine Corwin', 'hayley.crooks@example.org', 'Carrie Cartwright', 'Dr. Estelle Olson', 'Myra Corwin', 'Prof. Sally Bernhard', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 104),
(105, 'Prof. Tyree Corkery Jr.', 'Vinnie Vandervort', 'Prof. Torrey Hauck V', 'sbergnaum@example.org', 'Camden Daugherty PhD', 'Stephan Satterfield', 'Dr. Arnaldo Shanahan PhD', 'Prof. Terrence Lockman MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 105),
(106, 'Elizabeth Labadie', 'Sandra Ryan', 'Faye Daugherty', 'enoch.blick@example.net', 'Rubie Bernhard', 'Mrs. Daisy Krajcik', 'Dr. Myrl Cremin DDS', 'Mr. Devyn McClure III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 106),
(107, 'Orpha Pacocha', 'Miss Connie Deckow DVM', 'Amir McCullough I', 'marie81@example.org', 'Mr. Alexandre Jacobi I', 'Kavon Hartmann PhD', 'Mrs. Thora Torp', 'Isai Ebert Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 107),
(108, 'Parker Leffler', 'Arvid Gorczany', 'Felipa Sauer', 'yazmin92@example.com', 'Mina Towne', 'Gus Metz', 'Marjolaine Fadel', 'Eric Jast', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 108),
(109, 'Vernie Gerhold', 'Prof. Tania Kshlerin V', 'Mrs. Sharon Osinski', 'denesik.dianna@example.com', 'Brannon Effertz Jr.', 'Luisa Huels', 'Porter Waelchi', 'Alysha Denesik', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 109),
(110, 'Alyce Graham', 'Miss Lola Bosco MD', 'Lori Emard', 'winfield04@example.net', 'Arno Schamberger IV', 'Carolyn Schoen', 'Mrs. Breanna Larkin', 'Lou Ullrich', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 110),
(111, 'Jacky Osinski DDS', 'Prof. Colleen Spinka', 'Dr. Cassandre Anderson IV', 'ngutkowski@example.org', 'Ms. Mae Kuhic', 'Mr. Angel Jacobi', 'Prof. Tanner Kemmer', 'Jaiden Blanda', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 111),
(112, 'Genevieve Labadie', 'Andre Buckridge', 'Tierra Hauck', 'thiel.moises@example.org', 'Dr. Erick Boyer', 'Rudolph Legros', 'Jadon Muller', 'Ms. Abigail Luettgen', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 112),
(113, 'Leonie Rohan DDS', 'Arely Hettinger MD', 'Nathanael Luettgen', 'bjaskolski@example.com', 'Elissa Hayes', 'Dr. Elton Zemlak', 'Karl Bruen PhD', 'Mr. Buford Larkin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 113),
(114, 'Israel Jacobson', 'Dr. Katherine Ruecker MD', 'Mr. Ian Nitzsche', 'eichmann.odell@example.org', 'Jammie Swift', 'Linda Brekke', 'Hermann Powlowski', 'Ollie Turner', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 114),
(115, 'Prof. Lesley Mayert', 'Maximillian Durgan V', 'Gilbert Krajcik', 'irving.harris@example.net', 'Prof. Antonia Donnelly', 'Amiya Abshire', 'Ms. Winona Gottlieb PhD', 'Arvel Schroeder', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 115),
(116, 'Prof. Itzel Gusikowski III', 'Edythe Schumm', 'Prof. Dagmar O\'Connell MD', 'tod.aufderhar@example.org', 'Herbert Larson Sr.', 'Prof. Gussie Hegmann', 'Julius Ernser II', 'Prof. Fay Schoen DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 116),
(117, 'Talon Nolan', 'Junior Rippin', 'Ara Bins', 'genoveva25@example.com', 'Prof. Doug Schimmel', 'Selina Nader', 'Jeffrey Toy MD', 'Dr. Yvette Hane', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 117),
(118, 'Mina Ankunding', 'Prof. Bryce Ward Sr.', 'Nannie Purdy', 'luis53@example.com', 'Elian Prosacco DVM', 'Dr. Aleen Bergnaum', 'Sarah Schiller', 'Ardella McCullough', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 118),
(119, 'Mr. Dayne Rempel', 'Madalyn Spinka', 'Anika Hintz V', 'mhilpert@example.com', 'Emie Grady I', 'Mr. Alberto Boehm', 'Nicolas Kub', 'Jamel Ratke', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 119),
(120, 'Lina Mann', 'Miss Flossie Padberg DDS', 'Miguel Roob', 'lkris@example.net', 'Prof. Tomas Stiedemann', 'Shad Runolfsdottir Jr.', 'Casey Gislason', 'Maya Hackett DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 120),
(121, 'Kendrick Armstrong', 'Aida Kreiger', 'Prof. Prince Murphy PhD', 'mckenzie.billie@example.org', 'Felicita Kulas', 'Dr. Caden Johns DDS', 'Bryon Hessel', 'Alize Stamm', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 121),
(122, 'Prof. Raven Ratke Jr.', 'Prof. Talon Leannon', 'Jayme Sipes', 'mdibbert@example.org', 'Otha Prosacco', 'Carlee Muller', 'Miss Madelyn Herzog', 'Grayson Harber', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 122),
(123, 'Alf Williamson I', 'Evert Smitham', 'Adrianna Volkman', 'zackery.simonis@example.com', 'Dr. Randall Yundt Jr.', 'Keegan Bode', 'Allison Pacocha', 'Vena Reichel', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 123),
(124, 'Norris Wunsch V', 'Prof. Sim Zboncak DVM', 'Jalen Hyatt', 'shanna12@example.net', 'Dr. Wellington Stroman III', 'Amy Douglas', 'Rusty Kuhn', 'Elva Satterfield III', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 124),
(125, 'Vincenzo Feeney', 'Bailey Rosenbaum', 'Kamren Connelly', 'kraig29@example.org', 'Garth Kilback', 'Ariel Muller', 'Dr. Vilma Haley', 'Rose Mosciski', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 125),
(126, 'Clemmie Becker', 'Noemie Williamson', 'Mrs. Diana Dare Sr.', 'millie.ziemann@example.com', 'Lulu Dickinson', 'Reagan Miller', 'Lysanne McGlynn', 'Dell Streich', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 126),
(127, 'Aletha Krajcik', 'Shanel Borer', 'Bridie Rolfson', 'reilly87@example.net', 'Miss Giovanna Dicki MD', 'Prof. Jason Lebsack', 'Mr. Tyler Renner I', 'Kamryn Herzog MD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 127),
(128, 'Prof. Jordy Fay Jr.', 'Etha Moore', 'Dr. Eileen Brown III', 'jedediah05@example.org', 'Heidi Rau', 'Lillian Roob', 'Maximillian Kshlerin', 'Cooper Collins', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 128),
(129, 'Lester Wolff', 'Ludwig Bode', 'Dr. Curtis Bartoletti', 'kamille.schuppe@example.com', 'Brady Hickle', 'Sigrid Champlin', 'Jaydon Watsica', 'Darrel Oberbrunner', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 129),
(130, 'Cielo Purdy', 'Jody D\'Amore', 'Grace Lesch', 'austen92@example.com', 'Daisy Fritsch', 'Cruz Schmitt', 'Adriel Parker', 'Deontae Reilly I', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 130),
(131, 'Mr. Blaise McGlynn V', 'Pearl Farrell', 'Jeffery Stroman Jr.', 'uratke@example.org', 'Marilyne Bahringer III', 'Kaya Brakus II', 'Mertie Bechtelar', 'Karson Emmerich', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 131),
(132, 'Adonis Goldner', 'Clark Cole', 'Mrs. Lou Schinner', 'ernesto.labadie@example.org', 'Hailie King', 'Ms. Heath Homenick V', 'Prof. Broderick Ebert DVM', 'Carroll Gorczany', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 132),
(133, 'Justus Zemlak', 'Ophelia Bartoletti', 'Miss Ara Fritsch Jr.', 'darwin.klein@example.org', 'Stanton Bruen', 'Francis Howell Sr.', 'Gia Buckridge III', 'Alexander Hettinger', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 133),
(134, 'Henri Heidenreich', 'Prof. Simeon Mayer Jr.', 'Raphael Price', 'muriel.vonrueden@example.net', 'Elsa Kovacek Sr.', 'Miss Josefina Funk', 'Hester Upton', 'Dewitt Mertz', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 134),
(135, 'Willard Gottlieb', 'Trevion Haag III', 'Dr. Steve Yost III', 'mayer.carrie@example.net', 'Prof. Name Klocko', 'Marguerite Bayer', 'Freida Koch', 'Dr. Raheem Leffler', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 135),
(136, 'Ms. Velma Heidenreich', 'Hobart Hayes II', 'Aliyah Walker MD', 'xconroy@example.net', 'Keira Kreiger IV', 'Jarvis Collier', 'Nichole Daugherty', 'Miss Selina Robel V', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 136),
(137, 'May Krajcik', 'Miss Sophie Daniel II', 'Prof. Owen Olson', 'maya.kassulke@example.net', 'Mohamed Rau', 'Prof. Manley Rath V', 'Miss Cristal Heidenreich', 'Wilfred Adams II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 137),
(138, 'Prof. Ari Mraz DVM', 'Scotty Kirlin', 'Stevie Langworth Jr.', 'nkreiger@example.net', 'Ms. Tiara Quigley', 'Tyrel Anderson', 'Baylee Nader Sr.', 'Mr. Lennie Schinner II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 138),
(139, 'Oral Heaney MD', 'Fritz Bahringer', 'Jean Hoeger', 'heidenreich.betty@example.org', 'Daphney Strosin V', 'Carmelo Krajcik', 'Emmanuel Pacocha PhD', 'Marlee Hintz DDS', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 139),
(140, 'Jewell Harris', 'Mr. Jayden Bosco', 'Prof. Brielle Renner', 'bechtelar.hershel@example.net', 'Heath Bailey IV', 'Lessie Feil', 'Odie Purdy', 'Marcelo Littel', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 140),
(141, 'Mr. Bart Keeling', 'Heidi Batz', 'Cortney Ledner', 'blanche56@example.org', 'Norene Conn', 'Horacio Harvey', 'Miss Alanis Greenfelder', 'Dr. Nola Hills Sr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 141),
(142, 'Jazlyn Ritchie', 'Ms. Evalyn Simonis Sr.', 'Dr. Coleman Hickle IV', 'aniya13@example.org', 'Scarlett Stehr', 'Zachariah Schaden DVM', 'Jailyn Lakin III', 'Prof. William Gulgowski', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 142),
(143, 'Lauretta Ernser', 'George Towne PhD', 'Coleman Champlin', 'cydney50@example.org', 'Domenica Gorczany', 'Muriel Baumbach DVM', 'Aileen Bednar V', 'Mrs. Marguerite Kihn I', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 143),
(144, 'Dangelo Jast', 'Dayne Greenholt', 'Vernie Weimann', 'kautzer.albert@example.org', 'Keven Stroman I', 'Tremayne Douglas IV', 'Mrs. Kaycee Schultz II', 'Prof. Athena King DDS', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 144),
(145, 'Isabelle O\'Kon', 'Lia Konopelski', 'Ms. Arielle Lubowitz III', 'gulgowski.arlie@example.net', 'Lexie Friesen', 'Don Reichel', 'Mrs. Betsy Murphy', 'Tremaine Satterfield', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 145),
(146, 'Kasey Hermann', 'Mr. Alexzander Beahan', 'Brittany Blanda', 'bergnaum.adela@example.com', 'Dr. Enoch Bernhard', 'Xzavier Abernathy', 'Ernie Hane', 'Miss Lina Oberbrunner', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 146),
(147, 'Mr. Watson Gerhold', 'Chasity Dare', 'Chaim Bartell', 'anabel.greenholt@example.net', 'Mrs. Enola Douglas', 'Xander Hegmann', 'Anahi Rutherford', 'Gilbert Volkman', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 147),
(148, 'Brendan Stoltenberg', 'Joel Anderson', 'Cleve Tromp', 'kenna60@example.org', 'Kory Grant', 'Chad Shields III', 'Mrs. Joana Lynch', 'Alexandrine Mills', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 148),
(149, 'Prof. Carlee McGlynn DDS', 'Mr. Garry Breitenberg', 'Dr. Noe Wisozk', 'wwiegand@example.org', 'Ms. Elisa Douglas Jr.', 'Nova Quigley', 'Dr. Xavier Metz DVM', 'Dr. Katrina Schmitt', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 149),
(150, 'Mrs. Hattie Gleason I', 'Eloisa Weissnat', 'Clarabelle Casper', 'fjohns@example.net', 'Maryjane Larson', 'Mr. Lemuel McKenzie V', 'Miss Freda Rogahn II', 'Audreanne Nikolaus', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 150);

-- --------------------------------------------------------

--
-- Table structure for table `client_statuses`
--

DROP TABLE IF EXISTS `client_statuses`;
CREATE TABLE IF NOT EXISTS `client_statuses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_statuses_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM AUTO_INCREMENT=151 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `client_statuses`
--

INSERT INTO `client_statuses` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jada Ritchie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(2, 'Dr. Jayson Murazik Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(3, 'Ramiro Konopelski', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(4, 'Prof. Alva Dare PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(5, 'Mr. Reuben Lebsack IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(6, 'Buford Pfeffer', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(7, 'Dr. Taylor Anderson PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(8, 'Mr. Robin Schmeler DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(9, 'Scot Bauch IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(10, 'Jude Fahey DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(11, 'Prof. Sincere Stamm', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(12, 'Dr. Freddie Green Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(13, 'Camylle Baumbach', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(14, 'Tre Towne', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(15, 'Elinore Ratke', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(16, 'Emilie Hodkiewicz PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(17, 'Tod Stracke', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(18, 'Mr. Scot Mante V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(19, 'Allene Abernathy', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(20, 'Theron Steuber III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(21, 'Reece Oberbrunner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(22, 'Thalia Hansen', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(23, 'Lenny Ziemann Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(24, 'Prof. Margarita Frami II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(25, 'Retha Tromp', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(26, 'Alanis Spinka', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(27, 'Pierre Blick', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(28, 'Gisselle Davis', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(29, 'Ms. Adah Dibbert', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(30, 'Thelma Yundt', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(31, 'Prof. Isac Schaden', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(32, 'Dr. Blaise Mills', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(33, 'Salma Walsh', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(34, 'Reyes Collier', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(35, 'Cicero Zemlak Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(36, 'Prof. Adelbert Dibbert PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(37, 'Dr. Tracy Moen', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(38, 'Berenice Daniel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(39, 'Aliza Bernhard III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(40, 'Stanley Berge', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(41, 'Rebeca Koepp', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(42, 'Ms. Elizabeth Wuckert Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(43, 'Blair McClure', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(44, 'Cicero Wunsch Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(45, 'Misty Sipes V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(46, 'Edwina Franecki', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(47, 'Mrs. Jacinthe Strosin V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(48, 'Prof. Alicia Schinner III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(49, 'Rosanna Sauer DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(50, 'Orlando Kuhic', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(51, 'Hank Kunde', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(52, 'Emmett Casper', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(53, 'Alexandrea O\'Keefe', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(54, 'Mr. Domenic Beatty', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(55, 'Ms. Zelda Heidenreich I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(56, 'Selena Hirthe', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(57, 'Elna Bruen', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(58, 'Euna Hirthe', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(59, 'Kadin Balistreri', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(60, 'Miss Laurence Bayer I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(61, 'Dr. Gunner Beier', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(62, 'Prof. Remington O\'Keefe', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(63, 'Dr. Brady Armstrong', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(64, 'Prof. Rylan Greenfelder V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(65, 'Ms. Dandre Runte I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(66, 'Miss Ethelyn Swift', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(67, 'Mario Williamson Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(68, 'Anya Larson V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(69, 'Prof. Archibald Treutel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(70, 'Timmothy Kuhic', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(71, 'Prof. Jared Huel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(72, 'Dr. Dedrick Kling DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(73, 'Jackson Lowe', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(74, 'Mrs. Cortney Luettgen V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(75, 'Zaria Crooks', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(76, 'Alivia Barrows', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(77, 'Laurine DuBuque', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(78, 'Mr. Ernest Deckow', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(79, 'Ciara Goldner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(80, 'Edwin Huels', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(81, 'Lula DuBuque', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(82, 'Prof. Tanner Considine II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(83, 'Tyson Lebsack Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(84, 'Pascale Rodriguez V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(85, 'Ed Murazik II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(86, 'Jocelyn Kuhn', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(87, 'Matilda Labadie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(88, 'Prof. Corine Schroeder I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(89, 'Shad Trantow', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(90, 'Gene Bailey', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(91, 'Dr. Stanley Lindgren', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(92, 'Terry Boehm IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(93, 'Bernie Shields', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(94, 'Mr. Gideon Hansen PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(95, 'Benjamin Jast IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(96, 'Prof. Carmine Schinner I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(97, 'Celestine Rowe DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(98, 'Judson Heaney', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(99, 'Willard Weber', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(100, 'Friedrich Sauer', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(101, 'Ova Bode', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(102, 'Delphine Erdman IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(103, 'Johnson Hessel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(104, 'Ms. Sharon Beer', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(105, 'Jackeline Toy', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(106, 'Miguel Parker', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(107, 'Maureen Schmeler', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(108, 'Mrs. Jazlyn Bahringer', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(109, 'Clare Halvorson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(110, 'Doug Brakus DDS', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(111, 'Rosie Gleason', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(112, 'Rosie Okuneva', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(113, 'Mrs. Amalia Barrows Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(114, 'Ms. Kelsie Swaniawski III', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(115, 'Dessie Corkery', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(116, 'Mr. Benedict Strosin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(117, 'Rory Thompson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(118, 'Prof. Florence Jakubowski', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(119, 'Mrs. Leola Howe', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(120, 'Mallie Kessler', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(121, 'Miss Leilani Howell I', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(122, 'Forrest Lindgren', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(123, 'Antonio Herzog', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(124, 'Ms. Michelle Simonis', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(125, 'Maurice Hermiston', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(126, 'Bethel Will', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(127, 'Mrs. Joanie Reinger', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(128, 'Efrain Ruecker', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(129, 'Willow Hermann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(130, 'Ms. Linnea Wisozk', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(131, 'Ms. Dixie Zemlak Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(132, 'Mrs. Aileen Kozey', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(133, 'Prof. Cade Kreiger', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(134, 'Sage Crist', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(135, 'Mrs. Karolann Marvin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(136, 'Mrs. Elna Cronin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(137, 'Ella Renner', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(138, 'Napoleon Graham', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(139, 'Kailyn Schmitt', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(140, 'Thomas Eichmann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(141, 'Hilbert Dare DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(142, 'Theresia Gislason', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(143, 'Turner Pagac', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(144, 'Isabelle Huels Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(145, 'Dr. Abel Considine Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(146, 'Willow Kuhlman', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(147, 'Danika Sporer', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(148, 'Dorothy Hand II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(149, 'Mr. Pietro Dach II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(150, 'Eric Bednar', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_currency` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currencies_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `title`, `code`, `main_currency`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'USD', 'USD', 1, '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(2, 'EUR', 'EUR', 0, '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(3, 'GBP', 'GBP', 0, '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `documents_deleted_at_index` (`deleted_at`),
  KEY `5347_5b20f7978ca89` (`project_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `income_sources`
--

DROP TABLE IF EXISTS `income_sources`;
CREATE TABLE IF NOT EXISTS `income_sources` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_percent` double(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `income_sources_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `income_sources`
--

INSERT INTO `income_sources` (`id`, `title`, `fee_percent`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Upwork', 10.00, '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(2, 'Direct client', 0.00, '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(3, 'Wilbert Wintheiser', 64.06, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(4, 'Aiyana Goodwin', 19.49, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(5, 'Miss Tressie Kulas IV', 95.51, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(6, 'Mrs. Catalina Hackett', 6.56, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(7, 'Camryn Gutkowski DVM', 61.04, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(8, 'Dr. Giuseppe McCullough', 25.18, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(9, 'Dr. Stephania Mueller PhD', 86.30, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(10, 'Tressie Aufderhar', 94.81, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(11, 'Marlen Cronin', 26.71, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(12, 'Dr. Josiane Gibson', 22.97, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(13, 'Mrs. Kira O\'Conner', 55.95, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(14, 'Hiram Glover', 79.01, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(15, 'Eveline Bogisich', 38.12, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(16, 'Mrs. Maybell Torphy II', 30.44, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(17, 'Mariano Orn', 54.01, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(18, 'Katrina Veum', 72.35, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(19, 'Mr. Robert Dickens I', 68.18, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(20, 'Mr. Colby Balistreri DDS', 35.71, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(21, 'Petra Hirthe PhD', 73.43, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(22, 'Lurline Emard Jr.', 75.40, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(23, 'Timmy Rempel II', 25.49, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(24, 'Ludwig Bernier', 47.53, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(25, 'Elody Jenkins', 83.71, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(26, 'Prof. Savanah Stroman', 70.03, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(27, 'Prof. Saige Durgan', 74.20, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(28, 'Eduardo Borer', 50.50, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(29, 'Weldon Casper DDS', 18.56, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(30, 'Katlynn Carroll', 8.00, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(31, 'Bridget Dooley', 89.10, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(32, 'Millie Dooley', 45.32, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(33, 'Andres Schamberger II', 53.19, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(34, 'Lavina Labadie', 41.12, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(35, 'Denis Kuvalis', 38.64, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(36, 'Jacklyn Jacobs', 5.74, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(37, 'Prof. Kacey Prosacco Jr.', 92.88, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(38, 'Mrs. Stephany Gibson', 27.69, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(39, 'Leonor Franecki', 74.45, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(40, 'Clementine Nicolas', 48.46, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(41, 'Mr. Don Brown', 3.03, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(42, 'Prof. Jaylon Metz', 26.34, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(43, 'Granville Kunze', 75.12, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(44, 'Gust Tromp', 78.31, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(45, 'Justus Nienow', 35.84, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(46, 'Travis Mraz', 39.73, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(47, 'Prof. George Ondricka', 23.44, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(48, 'Unique Stanton', 70.84, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(49, 'Guillermo Reichert', 96.30, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(50, 'Anibal Mertz', 34.94, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(51, 'Enrico Gorczany', 64.72, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(52, 'Horace Kreiger', 25.73, '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(32, '2014_10_12_000000_create_users_table', 1),
(33, '2014_10_12_100000_create_password_resets_table', 1),
(34, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(35, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(36, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(37, '2016_06_01_000004_create_oauth_clients_table', 1),
(38, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(39, '2016_11_01_093100_create_user_actions_table', 1),
(40, '2018_06_13_133922_create_1528886362_permissions_table', 1),
(41, '2018_06_13_133923_create_1528886363_roles_table', 1),
(42, '2018_06_13_133923_create_1528886363_users_table', 1),
(43, '2018_06_13_133927_create_5b20f45b4a2d3_permission_role_table', 1),
(44, '2018_06_13_133927_create_5b20f45c0e733_role_user_table', 1),
(45, '2018_06_13_134048_create_1528886448_currencies_table', 1),
(46, '2018_06_13_134136_create_1528886496_transaction_types_table', 1),
(47, '2018_06_13_134245_create_1528886565_income_sources_table', 1),
(48, '2018_06_13_134335_create_1528886615_client_statuses_table', 1),
(49, '2018_06_13_134428_create_1528886668_project_statuses_table', 1),
(50, '2018_06_13_134717_create_1528886836_clients_table', 1),
(51, '2018_06_13_134718_add_5b20f635608c9_relationships_to_client_table', 1),
(52, '2018_06_13_134958_create_1528886997_projects_table', 1),
(53, '2018_06_13_134959_add_5b20f6d5dbb7d_relationships_to_project_table', 1),
(54, '2018_06_13_135057_create_1528887056_notes_table', 1),
(55, '2018_06_13_135058_add_5b20f71118214_relationships_to_note_table', 1),
(56, '2018_06_13_135120_add_5b20f72860630_relationships_to_project_table', 1),
(57, '2018_06_13_135311_create_media_table', 1),
(58, '2018_06_13_135312_create_1528887191_documents_table', 1),
(59, '2018_06_13_135313_add_5b20f797a9a56_relationships_to_document_table', 1),
(60, '2018_06_13_135540_create_1528887339_transactions_table', 1),
(61, '2018_06_13_135541_add_5b20f82b972a3_relationships_to_transaction_table', 1),
(62, '2018_06_13_135606_add_5b20f846cf6ab_relationships_to_client_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `note_text` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notes_deleted_at_index` (`deleted_at`),
  KEY `5346_5b20f710f288d` (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `note_text`, `created_at`, `updated_at`, `deleted_at`, `project_id`) VALUES
(1, 'Diana Donnelly', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 1),
(2, 'Iliana Renner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 2),
(3, 'Giovani Considine IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 3),
(4, 'Bella Ziemann', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 4),
(5, 'Name Kuhic', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 5),
(6, 'Mr. Jaren Crist V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 6),
(7, 'Javier Barton II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 7),
(8, 'Prof. Ezequiel Gerhold', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 8),
(9, 'Oma Pacocha', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 9),
(10, 'Elenor Rosenbaum', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 10),
(11, 'Wallace Huels', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 11),
(12, 'Erin Weimann', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 12),
(13, 'Clemens Cole I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 13),
(14, 'Drew Rice', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 14),
(15, 'Jeramy Stiedemann II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 15),
(16, 'Deshawn McKenzie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 16),
(17, 'Dr. Delores Berge DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 17),
(18, 'Isabel Hane', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 18),
(19, 'Cora Nicolas', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 19),
(20, 'Delia Wiza', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 20),
(21, 'Cara Bednar', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 21),
(22, 'Jovany Stanton', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 22),
(23, 'Dr. Julien Schimmel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 23),
(24, 'Terry Jenkins', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 24),
(25, 'Cindy Brakus', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 25),
(26, 'Corine Ritchie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 26),
(27, 'Mr. Francis Beatty PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 27),
(28, 'Asa Rempel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 28),
(29, 'Gayle Johnson', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 29),
(30, 'Kaela Flatley', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 30),
(31, 'Anne Reichert', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 31),
(32, 'Prof. Enos Gerlach', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 32),
(33, 'Rita Keebler PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 33),
(34, 'Ms. Concepcion Waelchi', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 34),
(35, 'Camila Kunze', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 35),
(36, 'Stephen Will', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 36),
(37, 'Meagan Cartwright', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 37),
(38, 'Raymond Hoeger IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 38),
(39, 'Ms. Sister Heathcote IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 39),
(40, 'Moriah Hagenes', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 40),
(41, 'Mervin Adams', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 41),
(42, 'Efrain Grant', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 42),
(43, 'Miss Shemar Nolan', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 43),
(44, 'Maia Wisoky', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 44),
(45, 'Myriam Okuneva Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 45),
(46, 'Creola Pfannerstill DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 46),
(47, 'Prof. Lila Hand', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 47),
(48, 'Stanton Ankunding', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 48),
(49, 'Al McClure PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 49),
(50, 'Amari Shanahan', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 50);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Larancer Personal Access Client', 'irzTfSROl2D839RdoBLx6i9h8XagBOlRaIw7KYl1', 'http://localhost', 1, 0, 0, '2019-12-11 02:06:43', '2019-12-11 02:06:43'),
(2, NULL, 'Larancer Password Grant Client', 'wu6zpNRLWEVKSf9li9lAAPKMNI2tnASCUJP00gIT', 'http://localhost', 0, 1, 0, '2019-12-11 02:06:43', '2019-12-11 02:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-12-11 02:06:43', '2019-12-11 02:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'user_management_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(2, 'permission_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(3, 'permission_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(4, 'permission_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(5, 'permission_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(6, 'permission_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(7, 'role_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(8, 'role_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(9, 'role_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(10, 'role_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(11, 'role_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(12, 'user_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(13, 'user_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(14, 'user_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(15, 'user_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(16, 'user_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(17, 'setting_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(18, 'currency_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(19, 'currency_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(20, 'currency_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(21, 'currency_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(22, 'currency_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(23, 'transaction_type_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(24, 'transaction_type_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(25, 'transaction_type_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(26, 'transaction_type_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(27, 'transaction_type_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(28, 'income_source_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(29, 'income_source_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(30, 'income_source_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(31, 'income_source_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(32, 'income_source_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(33, 'client_status_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(34, 'client_status_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(35, 'client_status_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(36, 'client_status_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(37, 'client_status_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(38, 'project_status_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(39, 'project_status_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(40, 'project_status_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(41, 'project_status_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(42, 'project_status_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(43, 'client_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(44, 'client_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(45, 'client_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(46, 'client_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(47, 'client_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(48, 'project_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(49, 'project_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(50, 'project_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(51, 'project_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(52, 'project_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(53, 'note_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(54, 'note_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(55, 'note_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(56, 'note_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(57, 'note_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(58, 'document_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(59, 'document_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(60, 'document_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(61, 'document_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(62, 'document_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(63, 'transaction_access', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(64, 'transaction_create', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(65, 'transaction_edit', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(66, 'transaction_view', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(67, 'transaction_delete', '2019-12-11 02:06:34', '2019-12-11 02:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED DEFAULT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `fk_p_5335_5336_role_permi_5b20f45b4a39f` (`permission_id`),
  KEY `fk_p_5336_5335_permission_5b20f45b4a3e4` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(17, 2),
(18, 2),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 2),
(24, 2),
(25, 2),
(26, 2),
(27, 2),
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(50, 2),
(51, 2),
(52, 2),
(53, 2),
(54, 2),
(55, 2),
(56, 2),
(57, 2),
(58, 2),
(59, 2),
(60, 2),
(61, 2),
(62, 2),
(63, 2),
(64, 2),
(65, 2),
(66, 2),
(67, 2);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `budget` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `client_id` int(10) UNSIGNED DEFAULT NULL,
  `project_status_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projects_deleted_at_index` (`deleted_at`),
  KEY `5345_5b20f6d5b3ca2` (`client_id`),
  KEY `5345_5b20f6d5b8a0f` (`project_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `start_date`, `budget`, `created_at`, `updated_at`, `deleted_at`, `client_id`, `project_status_id`) VALUES
(1, 'Bernardo Marvin', 'Ludie Towne', '1983-07-17', 'Prof. Anika Rutherford', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 51, 1),
(2, 'Annetta Carter', 'Mr. Elroy Greenfelder', '2019-01-28', 'Tracey Cremin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 52, 2),
(3, 'Lucio Terry', 'Jeromy Ruecker', '1986-02-22', 'Jailyn Cassin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 53, 3),
(4, 'Estell Bartoletti', 'Marielle Kohler', '1997-11-19', 'Linnea Mosciski', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 54, 4),
(5, 'Ahmed Eichmann', 'Miss Betsy Nienow', '1980-03-25', 'Mr. Edwardo Waters IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 55, 5),
(6, 'Cleta Herzog', 'Anastasia Gerlach', '1971-01-27', 'Prof. Brandon Bruen', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 56, 6),
(7, 'Colleen Bednar', 'Carol Batz', '1971-03-13', 'Ivory Fay V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 57, 7),
(8, 'Kole Roberts I', 'Jayde Cormier I', '1973-05-15', 'Krystal Bergstrom MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 58, 8),
(9, 'Mrs. Cali Tillman I', 'Jovanny Ferry', '1974-03-08', 'Cyril Schultz', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 59, 9),
(10, 'Amya Upton', 'Dr. Hadley Langosh', '2011-08-01', 'Okey McClure Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 60, 10),
(11, 'Rebekah Koch', 'Mr. Elmer Hintz', '1977-04-26', 'Rosa Dibbert', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 61, 11),
(12, 'Westley Bernier', 'Shaun Kassulke', '1997-11-26', 'Nova Lynch', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 62, 12),
(13, 'Laverna Walter', 'Prof. Lewis Hand MD', '1983-06-02', 'Helmer O\'Kon', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 63, 13),
(14, 'Dr. Makenna Parker', 'Prof. Eileen Veum III', '1973-12-23', 'Mrs. Lexi Ledner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 64, 14),
(15, 'Earnestine Kutch', 'Augustus Becker', '1997-09-12', 'Juliet Steuber', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 65, 15),
(16, 'Prof. Maya Marquardt I', 'Nellie Olson', '1986-04-16', 'Mr. Douglas Denesik IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 66, 16),
(17, 'Dr. Nicklaus Ankunding I', 'Delilah Lesch', '1980-07-31', 'Sandrine Wiza', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 67, 17),
(18, 'Mozelle Hansen', 'Mr. Jonas Kassulke', '2015-02-16', 'Nels Johnston', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 68, 18),
(19, 'Miss Claudie Wilkinson MD', 'Concepcion Bosco', '1985-01-03', 'Myron Corwin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 69, 19),
(20, 'Roscoe Anderson', 'Berry McKenzie', '1985-12-03', 'Prof. Nora Hand II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 70, 20),
(21, 'Lempi Borer', 'Prof. Jessika Upton', '1973-02-28', 'Hubert Yost', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 71, 21),
(22, 'Miss Ashleigh Kub', 'Joelle Senger Jr.', '1992-10-15', 'Richmond Bergnaum V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 72, 22),
(23, 'Dr. Zander Wunsch', 'Eulah Gleason DVM', '2002-03-03', 'Kevon Watsica', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 73, 23),
(24, 'Prof. Sophia Ullrich II', 'Agustina Orn MD', '2007-01-22', 'Silas Hudson IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 74, 24),
(25, 'Dr. Reed Ryan DDS', 'Justus Kessler', '2015-01-25', 'Hollie Harber', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 75, 25),
(26, 'Josefina Bartoletti', 'Ettie Schultz', '1977-02-26', 'Gail Keebler', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 76, 26),
(27, 'Clemmie Stanton DVM', 'Sabina Howe', '1999-12-07', 'Edison Goldner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 77, 27),
(28, 'Elyse Spencer', 'Floy Kertzmann', '1984-05-17', 'Otho Stracke', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 78, 28),
(29, 'Mrs. Citlalli Oberbrunner II', 'Enos Moore', '1980-09-24', 'Ms. Jada Durgan Jr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 79, 29),
(30, 'Kennedy Bogan', 'Benton Heidenreich', '1996-03-09', 'Juliana Powlowski', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 80, 30),
(31, 'Jaron Larson', 'Merl Gerlach', '2016-09-13', 'Dr. Elyssa Wisoky', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 81, 31),
(32, 'Cody Osinski', 'Tate Ruecker PhD', '1998-02-20', 'Dr. Novella Rau', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 82, 32),
(33, 'Bradford Kozey', 'Mr. Emmet Johnson PhD', '1985-12-16', 'Susan Feil', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 83, 33),
(34, 'Shanna Conn', 'Evelyn Hyatt', '2010-09-22', 'Lura Wunsch', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 84, 34),
(35, 'Griffin Mertz DVM', 'Ms. Tierra Gaylord', '1993-10-15', 'Hillary Hayes', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 85, 35),
(36, 'Jordon Yundt', 'Marquise Weber', '1981-06-01', 'Erik Romaguera', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 86, 36),
(37, 'Maurine Raynor IV', 'Justice Ebert', '2010-09-20', 'Wilford Bayer IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 87, 37),
(38, 'Prof. Laila Maggio', 'Prof. Lonzo Collier Sr.', '1973-12-10', 'Miss Shirley Fritsch', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 88, 38),
(39, 'Mr. Reilly Romaguera I', 'Kayden Dietrich', '1986-01-04', 'Cameron McDermott', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 89, 39),
(40, 'Lincoln Quigley V', 'Mr. Charlie Rosenbaum DDS', '2010-05-10', 'Lois Labadie Sr.', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 90, 40),
(41, 'Prof. Shany Hermann II', 'Alan Kris', '1986-05-13', 'Martina Kshlerin III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 91, 41),
(42, 'Gerardo Dickinson', 'Mrs. Emmie Lowe V', '2006-01-27', 'Miss Mia Spencer DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 92, 42),
(43, 'Helga Marquardt', 'Theodore Fadel', '2006-05-25', 'Corene Feest', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 93, 43),
(44, 'Rachel Steuber', 'Dr. Laurianne Ernser V', '1971-10-24', 'Keyon Wintheiser', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 94, 44),
(45, 'Emil Kautzer', 'Adriana Boyle DVM', '1989-11-03', 'Dr. Adonis Nikolaus', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 95, 45),
(46, 'Myrl Gerlach', 'Tia Bashirian', '2013-05-06', 'Prof. Moshe Jast V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 96, 46),
(47, 'Liliana Sporer', 'Mr. Clemens Runolfsdottir I', '1993-02-16', 'Winona Weber', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 97, 47),
(48, 'Zelda Miller', 'Cindy Buckridge', '2019-02-25', 'Orie Ryan', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 98, 48),
(49, 'Zoe Veum V', 'Orval Miller', '2014-09-08', 'Tavares Tromp', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 99, 49),
(50, 'Sarah Cole', 'Anastacio Feil', '1981-02-08', 'Flo VonRueden', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL, 100, 50),
(51, 'Candelario Harris', 'Prof. Addison Jacobi', '1983-05-17', 'Chanel Cummerata', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 101, 54),
(52, 'Miss Aubree Grimes III', 'Mrs. Gerry Mante', '2013-02-26', 'Roxane Willms', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 102, 55),
(53, 'Ramiro Huel', 'Norwood Huel Jr.', '1975-03-04', 'Myra Harber', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 103, 56),
(54, 'Jack Carroll', 'Prof. Clair Mann Sr.', '2014-12-05', 'Burley Welch', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 104, 57),
(55, 'Greta Hayes', 'Miss Rosa Kub', '2011-09-20', 'Penelope Shanahan', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 105, 58),
(56, 'Prof. Abdullah Powlowski', 'Ms. Rosamond Harber', '1977-03-08', 'Johathan Aufderhar', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 106, 59),
(57, 'Markus Mayert I', 'Kay Fay', '1970-06-09', 'Cleve Barrows', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 107, 60),
(58, 'Rosella Dooley', 'Jordane Mosciski', '1987-04-13', 'Dr. Brigitte Wintheiser', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 108, 61),
(59, 'Nikki Kihn', 'Dr. Filomena Spencer', '1974-01-11', 'Retta Walter', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 109, 62),
(60, 'Mrs. Alvera Miller Jr.', 'Mr. Damon Bradtke', '1975-08-07', 'Brandy Hoppe', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 110, 63),
(61, 'Erika Schuster', 'Ms. Mossie Ruecker PhD', '1977-10-11', 'Dr. Marjolaine Kuhn Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 111, 64),
(62, 'Tressa Larkin', 'Prof. Andres Daugherty', '1995-11-11', 'Dr. Jaunita Bergnaum PhD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 112, 65),
(63, 'Adrain Larson', 'Carley Purdy', '2006-09-07', 'Duane Ratke', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 113, 66),
(64, 'Monserrat Rath', 'Prof. Darius Dibbert', '1978-05-07', 'Mr. Thurman Sipes', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 114, 67),
(65, 'Abner Hilpert', 'Kayla Crooks', '1981-06-23', 'Ms. Karelle Predovic', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 115, 68),
(66, 'Wilma Kerluke', 'Kaci Shanahan', '1978-11-29', 'Shane Pagac', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 116, 69),
(67, 'Eula Stark', 'Prof. Anastacio Morissette', '1976-01-24', 'Mason Kutch', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 117, 70),
(68, 'Kailyn Hoppe', 'Eden Rosenbaum', '1994-03-27', 'Monserrate Carter', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 118, 71),
(69, 'Destany Goyette', 'Laurie Satterfield III', '1978-09-24', 'Thea Gaylord', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 119, 72),
(70, 'Johnathan Braun', 'Josephine Cole', '2013-09-26', 'Bartholome Jacobson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 120, 73),
(71, 'Margarett Cummings', 'Prof. Joshua Kuhlman Jr.', '2011-12-31', 'Destin Monahan', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 121, 74),
(72, 'Mr. Eloy Schinner', 'Maximillia Koelpin', '1997-10-17', 'Pietro Larson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 122, 75),
(73, 'Mr. Gabriel Hansen Sr.', 'Damien Greenholt', '2002-01-13', 'Ms. Liliana Erdman', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 123, 76),
(74, 'Viva Ryan', 'Shanny Weimann', '2006-01-05', 'Jammie Walker', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 124, 77),
(75, 'Modesto Strosin', 'Prof. Vita Wiegand', '2016-02-19', 'Preston Cormier', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 125, 78),
(76, 'Destini Rippin', 'Hilario Harris', '2005-07-01', 'Dr. Golda Effertz', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 126, 79),
(77, 'Issac Harris V', 'Isom Herman', '2004-04-18', 'Elinore Waelchi', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 127, 80),
(78, 'Catherine Flatley', 'Mr. Alejandrin Powlowski', '1984-11-11', 'Dr. Napoleon Marvin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 128, 81),
(79, 'Dewitt Gaylord', 'Dr. Katlynn Harber', '2006-03-13', 'Prof. Patrick Huels', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 129, 82),
(80, 'Dr. Vincent Hettinger PhD', 'Mr. Olaf Lynch', '2016-05-01', 'Stefan Abernathy', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 130, 83),
(81, 'Ilene Haag', 'Reilly Bartell', '1996-04-22', 'Ashtyn Strosin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 131, 84),
(82, 'Mr. Christian Rau IV', 'Raquel Marvin', '1991-08-17', 'Herminio Runolfsson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 132, 85),
(83, 'Lavina Barton', 'Mark Runolfsdottir', '2010-04-01', 'Prof. Lola Hirthe', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 133, 86),
(84, 'Danika Marquardt MD', 'Ervin Wuckert', '2002-07-23', 'Jacquelyn Zieme', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 134, 87),
(85, 'Lavonne Moore', 'Ms. Annabelle Bayer PhD', '1986-05-19', 'Braeden Kuphal', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 135, 88),
(86, 'Kelsi Littel MD', 'Mrs. Anita Schinner', '1995-06-08', 'Prof. Adrienne VonRueden', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 136, 89),
(87, 'Niko Labadie', 'Nestor Mante', '2011-08-23', 'Robert Bruen Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 137, 90),
(88, 'Anabel Adams', 'Dr. Carley Powlowski V', '2017-07-02', 'Bethel Zboncak', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 138, 91),
(89, 'Lloyd Larkin', 'Dr. Cyrus Jast II', '2006-12-17', 'Mina Lueilwitz DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 139, 92),
(90, 'Clifford Littel', 'Dr. Asha Becker', '2003-06-03', 'Prof. Connie Hermann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 140, 93),
(91, 'Lucinda Toy', 'Marlin Rogahn', '1979-04-29', 'Tatum Heaney', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 141, 94),
(92, 'Antonette Emard', 'Mr. Korey Runte', '1986-06-05', 'Gianni Kutch', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 142, 95),
(93, 'Mr. Chadd Morar PhD', 'Mckenzie Eichmann', '2001-07-10', 'Mariam Mayert', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 143, 96),
(94, 'Dallin Bogisich', 'Jesse Sanford', '1998-02-15', 'Marian Robel MD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 144, 97),
(95, 'Mr. Alden Schmeler', 'Shea Hintz', '2003-05-28', 'Samanta Hand', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 145, 98),
(96, 'Wilson Lynch', 'Mr. Maximillian Rowe I', '1990-02-24', 'Prof. Cyrus Aufderhar', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 146, 99),
(97, 'Mark Koepp', 'Noemy Shanahan', '1986-04-22', 'Brain Fritsch', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 147, 100),
(98, 'Felipe Wuckert', 'Corene Strosin', '2019-09-19', 'Nona McLaughlin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 148, 101),
(99, 'Lessie Hoppe', 'Alden Sporer', '1982-04-21', 'Daryl Stanton', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 149, 102),
(100, 'Jayde Bednar V', 'Neoma Stehr', '1983-01-28', 'Sierra Spinka IV', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 150, 103);

-- --------------------------------------------------------

--
-- Table structure for table `project_statuses`
--

DROP TABLE IF EXISTS `project_statuses`;
CREATE TABLE IF NOT EXISTS `project_statuses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_statuses_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_statuses`
--

INSERT INTO `project_statuses` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cornelius Hintz', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(2, 'Prof. Christina Nader', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(3, 'Arlo Bartoletti', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(4, 'Ignacio Sawayn', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(5, 'Alayna Daugherty', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(6, 'Breanne Wuckert I', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(7, 'Kaci Heidenreich', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(8, 'Claude Stehr', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(9, 'Ms. Fannie Jenkins', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(10, 'Cheyenne Thiel MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(11, 'Aaron McKenzie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(12, 'Bette Gorczany', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(13, 'Gunner Heathcote', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(14, 'Dr. Javonte Flatley', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(15, 'Miss Cathy Padberg', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(16, 'Garrick Yost', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(17, 'Mohammed Konopelski MD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(18, 'Adrain Zulauf PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(19, 'Arnold Cormier', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(20, 'Claudie Schamberger', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(21, 'Carlee Leuschke', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(22, 'Sally Paucek', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(23, 'Alvah Wiegand', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(24, 'Della Dicki V', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(25, 'Lilliana Wisoky', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(26, 'Prof. Columbus Larkin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(27, 'Ms. Tess Daniel', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(28, 'Waino Swaniawski DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(29, 'Rosalinda Corwin', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(30, 'Mathias Blanda', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(31, 'Murl Herman', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(32, 'Roderick Carter III', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(33, 'Dr. Noel Hintz', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(34, 'Linnie Runolfsdottir', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(35, 'Karlie Jast', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(36, 'Skyla Robel IV', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(37, 'Miguel Schultz II', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(38, 'Alysson Ryan DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(39, 'Ebony Schiller', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(40, 'Santino Toy', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(41, 'Hope Dickinson', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(42, 'Dr. Isai Gerlach', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(43, 'Graciela Kling', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(44, 'Albina Kunde', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(45, 'Ebba Conroy', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(46, 'Mr. Constantin Bauch DDS', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(47, 'Hal McDermott', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(48, 'Nicklaus Oberbrunner', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(49, 'Paula Kuphal', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(50, 'Zack Witting', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(51, 'Active', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(52, 'On hold', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(53, 'Inactive', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(54, 'Sheridan Swift', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(55, 'Shayna Emmerich', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(56, 'Annamae Bailey', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(57, 'Prof. Lawrence Ritchie', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(58, 'Elroy Gerhold DVM', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(59, 'Talia Gutmann', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(60, 'Mr. Juwan Wunsch PhD', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(61, 'Naomi Howe', '2019-12-11 02:06:34', '2019-12-11 02:06:34', NULL),
(62, 'Vilma Marquardt', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(63, 'Buck Wolff', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(64, 'Benny Crist', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(65, 'Cornell Wisozk', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(66, 'Kaleigh Crist', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(67, 'Miller Bartell', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(68, 'Pat Stokes', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(69, 'Eldridge Reilly', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(70, 'Miss Kelly Wolff Sr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(71, 'Prof. Madelyn Mayert PhD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(72, 'Madyson Rath', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(73, 'Tito Tromp MD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(74, 'Vicky McKenzie', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(75, 'Prof. Raul Smith', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(76, 'Prof. Tyshawn Jones DDS', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(77, 'Brenden Boehm', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(78, 'Santos Trantow', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(79, 'Iliana Klocko', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(80, 'Janelle Schroeder', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(81, 'Diana Okuneva IV', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(82, 'Arch Luettgen Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(83, 'Lydia Wunsch', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(84, 'Mr. Gregorio Fay Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(85, 'Trenton Crist', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(86, 'Hulda Kuphal', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(87, 'Kyra Predovic', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(88, 'Johnathan Rolfson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(89, 'Brandi Volkman', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(90, 'Olaf Jenkins', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(91, 'Prof. Agustin Mante III', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(92, 'Jimmie Moore', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(93, 'Prof. Wendell Kessler', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(94, 'Dr. Virginie Schinner DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(95, 'Enid Carter', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(96, 'Thad Gottlieb DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(97, 'Bertha Wisozk', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(98, 'Marjorie Smitham II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(99, 'Brennan Hintz', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(100, 'Emelie O\'Connell II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(101, 'Hortense Prohaska', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(102, 'Nyasia Satterfield', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(103, 'Jamaal Towne', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Administrator (can create other users)', '2019-12-11 02:06:34', '2019-12-11 02:06:34'),
(2, 'Simple user', '2019-12-11 02:06:34', '2019-12-11 02:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  KEY `fk_p_5336_5337_user_role_5b20f45c0e7de` (`role_id`),
  KEY `fk_p_5337_5336_role_user_5b20f45c0e81f` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `amount` double(15,2) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `transaction_type_id` int(10) UNSIGNED DEFAULT NULL,
  `income_source_id` int(10) UNSIGNED DEFAULT NULL,
  `currency_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_deleted_at_index` (`deleted_at`),
  KEY `5348_5b20f82b5aeb7` (`project_id`),
  KEY `5348_5b20f82b60970` (`transaction_type_id`),
  KEY `5348_5b20f82b665cd` (`income_source_id`),
  KEY `5348_5b20f82b6c61a` (`currency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `title`, `description`, `amount`, `transaction_date`, `created_at`, `updated_at`, `deleted_at`, `project_id`, `transaction_type_id`, `income_source_id`, `currency_id`) VALUES
(1, 'Rosario Brown', 'Jarod Blanda', 2.03, '1984-04-05', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 16, 3, 3, 2),
(2, 'Marisol Hagenes', 'Agustin Dare Sr.', 51.72, '1990-11-10', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 83, 4, 4, 2),
(3, 'Kylee Von PhD', 'Randal Cartwright', 43.12, '2017-08-25', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 83, 5, 5, 2),
(4, 'Prof. Ahmed McLaughlin', 'D\'angelo Schultz', 74.31, '2011-09-17', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 91, 6, 6, 2),
(5, 'Mercedes Muller', 'Prof. Keyshawn Rosenbaum', 46.03, '1970-11-02', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 94, 7, 7, 1),
(6, 'Dr. Neil Auer V', 'Hassie Grady', 97.71, '1989-04-20', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 18, 8, 8, 1),
(7, 'Mrs. Cecilia Bashirian', 'Dr. Urban Gerhold', 63.15, '2006-12-05', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 42, 9, 9, 3),
(8, 'Aurelio Harvey', 'Mr. Loy Bins', 50.32, '2016-01-21', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 17, 10, 10, 2),
(9, 'Mr. Deshaun Hills', 'Bettye Skiles DVM', 96.13, '1984-03-09', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 45, 11, 11, 3),
(10, 'Raul Yost', 'Mrs. Ada Greenfelder', 36.01, '2005-06-09', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 89, 12, 12, 2),
(11, 'Orin Von', 'Josh VonRueden', 75.34, '1984-07-05', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 3, 13, 13, 3),
(12, 'Pattie Kiehn', 'Prof. Tianna Mertz MD', 8.86, '1974-06-29', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 83, 14, 14, 3),
(13, 'Devonte Hansen', 'Fermin Wunsch', 9.13, '1973-04-10', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 96, 15, 15, 2),
(14, 'Mr. Hayden Hackett II', 'Roosevelt Haag I', 94.99, '1990-07-12', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 18, 16, 16, 3),
(15, 'Roel Weimann', 'Abe Von', 70.22, '2019-09-16', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 78, 17, 17, 3),
(16, 'Kadin Rogahn MD', 'Prof. Lennie Sawayn', 25.52, '1977-10-20', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 21, 18, 18, 3),
(17, 'Demario King', 'Gilbert Brown', 32.27, '1987-09-23', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 37, 19, 19, 1),
(18, 'Herminia Grady', 'Miss Camille Hauck DDS', 14.73, '2016-12-28', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 91, 20, 20, 3),
(19, 'Shayna Thompson', 'Bertrand Cole', 62.64, '1997-09-24', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 44, 21, 21, 3),
(20, 'Giovanna Gutkowski', 'Jody Cronin DDS', 35.74, '1976-05-23', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 34, 22, 22, 1),
(21, 'Ebony Sawayn Sr.', 'Leanne Kunde', 83.76, '2016-08-02', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 48, 23, 23, 2),
(22, 'Deborah Jakubowski', 'Frankie King DVM', 60.29, '2018-07-30', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 25, 24, 24, 2),
(23, 'Giovani Robel Jr.', 'Calista Frami', 91.50, '1973-04-02', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 49, 25, 25, 3),
(24, 'Harry Kuhlman', 'Jacynthe Klocko', 33.86, '1979-05-15', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 82, 26, 26, 2),
(25, 'Lester Olson', 'Avis Auer DDS', 29.23, '1979-04-10', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 3, 27, 27, 1),
(26, 'Mr. Rylan Fahey Sr.', 'Kelsi Kiehn', 99.08, '1980-03-01', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 80, 28, 28, 1),
(27, 'Conner Lockman', 'Jared Hartmann', 30.88, '2016-02-04', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 13, 29, 29, 2),
(28, 'Della Koelpin', 'Dr. Conrad Conroy Sr.', 53.12, '1970-10-20', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 28, 30, 30, 3),
(29, 'Antonette Powlowski', 'Prof. Alf Batz', 21.41, '2002-08-31', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 80, 31, 31, 3),
(30, 'Ms. Brenna Gutmann', 'Fernando Tremblay III', 9.44, '2010-05-06', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 38, 32, 32, 1),
(31, 'Ms. Romaine Cartwright Jr.', 'Mrs. Mariana Ziemann', 39.22, '2019-01-24', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 2, 33, 33, 1),
(32, 'Prof. Norberto Sanford', 'Serenity Dare', 13.38, '1995-06-06', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 45, 34, 34, 1),
(33, 'Herman Hettinger', 'Gregg Schmidt II', 37.28, '1975-06-29', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 47, 35, 35, 1),
(34, 'Nicola Hyatt', 'Olin Lebsack', 12.83, '1995-04-04', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 96, 36, 36, 2),
(35, 'Luna Glover IV', 'Karelle Zboncak', 91.41, '2001-09-16', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 55, 37, 37, 3),
(36, 'Mrs. Elfrieda Sipes', 'Marlee Brekke', 50.80, '1996-12-08', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 50, 38, 38, 3),
(37, 'Prof. Eveline Rice DVM', 'Jed Spinka', 94.95, '2003-04-16', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 92, 39, 39, 1),
(38, 'Brigitte Reynolds DVM', 'Murl Heidenreich', 21.22, '2015-06-11', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 59, 40, 40, 3),
(39, 'Icie Zboncak', 'Maryse Bode', 74.80, '1986-03-11', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 37, 41, 41, 3),
(40, 'Wilfred Gerhold', 'Prof. Emilio Predovic', 52.59, '1981-02-22', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 11, 42, 42, 1),
(41, 'Mrs. Emily Crona', 'Reyna Macejkovic', 18.25, '1970-02-04', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 7, 43, 43, 3),
(42, 'Keanu Deckow', 'Miss Lauryn O\'Connell I', 6.45, '2008-10-18', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 91, 44, 44, 1),
(43, 'Miss Leora Stracke', 'Lavern Bednar II', 61.42, '2010-09-25', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 49, 45, 45, 1),
(44, 'Jeromy Rolfson', 'Dante Streich', 3.78, '2009-03-15', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 43, 46, 46, 2),
(45, 'Irwin Murphy', 'Prof. Keyshawn Olson MD', 64.56, '1974-12-31', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 1, 47, 47, 2),
(46, 'Dayna Auer', 'Dr. Seamus Kuhlman', 33.19, '2007-04-27', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 9, 48, 48, 2),
(47, 'Eloise Strosin DVM', 'Orpha Turcotte DVM', 98.78, '2006-03-29', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 58, 49, 49, 2),
(48, 'Waylon Gibson Jr.', 'Treva Wehner', 35.55, '1986-04-08', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 10, 50, 50, 1),
(49, 'Shania Raynor', 'Dr. Lorenza Barrows', 91.81, '1979-09-29', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 79, 51, 51, 1),
(50, 'Prof. Juston Prohaska', 'Mr. Nickolas Lind DDS', 84.02, '2012-03-15', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL, 62, 52, 52, 1);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_types`
--

DROP TABLE IF EXISTS `transaction_types`;
CREATE TABLE IF NOT EXISTS `transaction_types` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_types_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaction_types`
--

INSERT INTO `transaction_types` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Freelance income', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(2, 'Taxes', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(3, 'Nicholaus Kemmer II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(4, 'Gregg White', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(5, 'Justina Torp', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(6, 'Miss Brandy Okuneva V', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(7, 'Hollie Dare', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(8, 'Hubert Upton', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(9, 'Tyson Ortiz V', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(10, 'Sarah Carter Sr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(11, 'Leora Cormier', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(12, 'Ernie Kiehn', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(13, 'Sammy Lind Sr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(14, 'Augustus Breitenberg', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(15, 'Josie Feeney', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(16, 'Zechariah Weimann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(17, 'Maurice Moore', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(18, 'Arvel McLaughlin', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(19, 'Prof. Felton Kozey II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(20, 'Dr. Sigmund Maggio', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(21, 'Laura Hegmann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(22, 'Gia Altenwerth', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(23, 'Trenton Moore', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(24, 'Amaya Gusikowski', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(25, 'Travon Weimann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(26, 'Delia Rodriguez Sr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(27, 'Kurt Steuber II', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(28, 'Prof. Aurelio Luettgen PhD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(29, 'Rossie Jacobi DVM', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(30, 'Adaline Mann', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(31, 'Augusta Schoen', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(32, 'Deja Rolfson PhD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(33, 'Dr. Carlos Abshire', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(34, 'Remington Johnston', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(35, 'Evelyn Hartmann Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(36, 'Hillary Jacobson', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(37, 'Prof. Shakira Emard PhD', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(38, 'Ronaldo Witting', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(39, 'Angelita Hahn DDS', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(40, 'Kiarra Reynolds', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(41, 'Delphia Raynor', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(42, 'Wilfred Daugherty', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(43, 'Cecelia Lowe', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(44, 'Abbigail Rogahn', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(45, 'Enoch Denesik', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(46, 'Declan Rohan', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(47, 'Prof. Althea Runolfsson Jr.', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(48, 'Prof. Jamal Beahan', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(49, 'Krystal Schumm', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(50, 'Prof. Jackson Crooks', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(51, 'Dr. Xzavier Deckow', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL),
(52, 'Pamela Simonis', '2019-12-11 02:06:35', '2019-12-11 02:06:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@admin.com', '$2y$10$/Spp1ZzQX7lKLl6Iqxi1CuisRVsBRd2M5sVxoV0DzcmteMezXLeze', '', '2019-12-11 02:06:34', '2019-12-11 02:06:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_actions`
--

DROP TABLE IF EXISTS `user_actions`;
CREATE TABLE IF NOT EXISTS `user_actions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_5_user_user_id_user_action` (`user_id`),
  KEY `user_actions_deleted_at_index` (`deleted_at`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_actions`
--

INSERT INTO `user_actions` (`id`, `user_id`, `action`, `action_model`, `action_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'updated', 'projects', 100, '2019-12-11 02:10:01', '2019-12-11 02:10:01', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
